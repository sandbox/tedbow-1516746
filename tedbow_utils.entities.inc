<?php
/**
 * field names for bundle
 * @param string $entity_type
 * @param string $bundle
 * @return array
 */
function tedbow_utils_get_entity_field_names($entity_type, $bundle){
  return array_keys(tedbow_utils_get_entity_fields($entity_type, $bundle));
}
/**
 * Returns all names of field collection fields for entity 
 * @param unknown_type $entity_type
 * @param unknown_type $bundle
 * @return multitype:unknown 
 */
function tedbow_utils_get_entity_field_collection_names($entity_type, $bundle){
  $fields =  array_keys(tedbow_utils_get_entity_fields($entity_type, $bundle));
  $collections = array();
  foreach ($fields as $field) {
    if(tedbow_utils_is_field_collection($field)){
      $collections[] = $field;
    }
  }
  return $collections;
}
/**
 * Get fields for a bundle
 * @param string $entity_type
 * @param string $bundle
 * @param boolean $get_global
 * @return array
 */
function tedbow_utils_get_entity_fields($entity_type, $bundle, $get_global = false){
  $entity_fields = &drupal_static(__FUNCTION__);
  if(!isset($entity_fields)){
    $entity_fields = array();
  }
  if(!isset($entity_fields["{$entity_type}_{$bundle}"])){
    $fields = field_read_instances(array('entity_type' => $entity_type, 'bundle' => $bundle));
    $field_names = array();
    $fields_name_keyed = array();
    foreach ($fields as $field){
      $fields_name_keyed[$field['field_name']] = $field;
    }
    $entity_fields["{$entity_type}_{$bundle}"] = $fields_name_keyed;
  }
  if($get_global){
    //check to see if global has been fetched
    foreach($entity_fields["{$entity_type}_{$bundle}"] as $field){
      //check to see if global has been fetched
      $global_fetched = isset($field['global_settings']);
      break;
    }
    if(!$global_fetched){
      $global_fields = field_read_fields(array('field_name' => array_keys($entity_fields["{$entity_type}_{$bundle}"])));
      foreach ($global_fields as $global_field) {
        $entity_fields["{$entity_type}_{$bundle}"][$global_field['field_name']]['global_settings'] = $global_field;
      }
    }
  }
  return $entity_fields["{$entity_type}_{$bundle}"];
}
/**
 * Check if an entity bundle contain a certain field
 * @param string $entity_type
 * @param string $bundle
 * @param string $field_name
 * @return boolean
 */
function tedbow_utils_entity_has_field($entity_type, $bundle, $field_name){
  return in_array($field_name, tedbow_utils_get_entity_field_names($entity_type, $bundle));
}
/**
 * Get entity ids having matching value in a field
 * @param string $entity_type
 * @param string$bundle
 * @param String $field_name
 * @param string $value
 * @param string $column
 * @param array $entity_ids
 * 	Limit to only these entity ids
 * @return array
 */
function tedbow_utils_find_entities_with_field_value($entity_type, $bundle, $field_name, $value, $column = 'value', $entity_ids = array()){
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type',$entity_type, '=');
  $query->entityCondition('bundle',$bundle, '=');
  $query->fieldCondition($field_name, $column, $value);
  if(!empty($entity_ids)){
    $query->entityCondition('entity_id',$entity_ids, 'IN');
  }
  $result = $query->execute();
  return array_keys(isset($result[$entity_type])?$result[$entity_type]:array());
}
/**
* Get entity that has matching value in a field
* @param string $entity_type
* @param string$bundle
* @param String $field_name
* @param string $value
* @param string $column
* @param array $entity_ids
* 	Limit to only these entity ids
* @return array
*/
function tedbow_utils_find_entity_with_field_value($entity_type, $bundle, $field_name, $value, $column = 'value'){
  $entities = tedbow_utils_find_entities_with_field_value($entity_type, $bundle, $field_name, $value, $column = 'value');
  return array_shift($entities);
}
/**
* Find entities that match given conditions
* @param array $conditions
* 	entity or property conditions
* @param array $field_conditions
* 	array keys are field keys
* 	array values condtions array with keys: values(required), operator, column,
* @return array
*/
function tedbow_utils_entities_with_props($conditions, $field_conditions = array()){
  $query = new EntityFieldQuery();
  foreach ($conditions as $condition => $value) {
    if(in_array($condition, array('entity_type', 'bundle', 'revision_id','entity_id'))){
      $query->entityCondition($condition, $value);
    }
    else{
      $query->propertyCondition($condition, $value);
    }
  }
  foreach ($field_conditions as $field_name => $field_condition) {
    $field_condition['column'] = isset($field_condition['column'])?$field_condition['column']:'value';
    if(!isset($field_condition['operator'])) {
      $field_condition['operator'] = is_array($field_condition['value'])?'IN':'=';
    }
    $query->fieldCondition($field_name, $field_condition['column'], $field_condition['value'], $field_condition['operator']);
  }
  $result = $query->execute();
  if(isset($result[$conditions['entity_type']])){
    return $result[$conditions['entity_type']];
  }else{
    return array();
  }
}
function tedbow_utils_get_object_field_names($entity){
  $field_names = array();
  foreach ($entity as $field_name => $field) {
    if(strstr($field_name, 'field_')){
      $field_names[] = $field_name;
    }
  }
  return $field_names;
}
/**
 * checks if a field name corresponds to a field_collection
 * @param unknown_type $field_name
 * @return boolean
 */
function tedbow_utils_is_field_collection($field_name){
  $field_collections = &drupal_static(__FUNCTION__);
  if(!isset($field_collections)){
    $field_collections = db_select('field_config', 'fc')
    ->fields('fc', array('field_name'))
    ->condition('type', 'field_collection', '=')->execute()->fetchCol();
  }
  return in_array($field_name, $field_collections);
}
function tedbow_utils_convert_field_collection_item($object, $new_item = FALSE){
  if($new_item){
    unset($object->item_id);
    $object->is_new = TRUE;
  }
  return new FieldCollectionItemEntity((array)$object);
}
/**
 * Replace all field_collection ids in an entity for the loaded field_collections
 * Enter description here ...
 * @param object $entity
 * @param string $entity_type
 * @param string $bundle
 */
function tedbow_utils_load_collections(&$entity, $entity_type = 'node', $bundle = NULL){
  if($entity_type == 'node'){
    $bundle = $entity->type;
  }
  $fields = tedbow_utils_get_entity_fields($entity_type, $bundle, TRUE);
  $field_names = array_keys($fields);
  foreach ($entity as $field_name => &$field) {
    if(in_array($field_name, $field_names)){
      //dealing with a field
      if(!empty($field) && $fields[$field_name]['global_settings']['type'] == 'field_collection'){
        foreach($field['und'] as $delta => $field_value){
          $field['und'][$delta] = array('entity' => field_collection_field_get_entity($field_value, $field_name));
          tedbow_utils_load_collections($field['und'][$delta]['entity'], 'field_collection_item', $field_name);
        }
      }
    }
  }
}